sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, JSONModel, formatter, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("com.arete.hedefbarkod.controller.Worklist", {

		formatter: formatter,

		onInit : function () {
			var oViewModel,
			demLogoImage = jQuery.sap.getModulePath("com.arete.hedefbarkod");

			oViewModel = new JSONModel({
				DemLogo: demLogoImage,
				Barcode: "",
				Stocks: [],
				fragment:"",
				enabledHedef:false
				
				
			});
			this.setModel(oViewModel, "worklistView");
			
		},
		onUpdateFinished : function (oEvent) {
			var sTitle,
				oTable = oEvent.getSource(),
				iTotalItems = oEvent.getParameter("total");
			if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
				sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
			} else {
				sTitle = this.getResourceBundle().getText("worklistTableTitle");
			}
			this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
		},
		onPressClear:function(){
			var oViewModel = this.getModel("worklistView");
			oViewModel.setProperty("/Barcode","");
			oViewModel.setProperty("/Stocks", "");

		},
		onSubmitDepoAdres: function () {
			var that = this,
			 oDataModel = this.getModel(),
			 oViewModel = this.getModel("worklistView"),
			 inptBarcode=oViewModel.getProperty("/Barcode"),
			 processtype=1;
			 if (inptBarcode === "" || inptBarcode === undefined) {
				sap.m.MessageToast.show("Barkoda ait adres bulunamadı!");
				return;
			}


			oDataModel.callFunction("/GetStorageLocStock", {
				urlParameters: {
					"Lgpla": inptBarcode,
					"ProcessType": processtype
				},
				success: function(oData)
				{

					oViewModel.setProperty("/Stocks", oData.results);
				},
				error: function(oData)
				{
					oViewModel.setProperty("/Barcode", "");
					sap.m.MessageToast.show("Barkoda ait adres bulunamadı!");
				}
				
			});
		},
		onOpenDialog:function(oEvent)
		{
			var oViewModel = this.getModel("worklistView"),
			satir=oEvent.getSource().getBindingContext("worklistView").getObject();
			oViewModel.setProperty("/fragment", satir);
			 if (!this.newDialog) {
				                this.newDialog =sap.ui.xmlfragment("com.arete.hedefbarkod.view.fragment.Dialog", this);
				                this.getView().addDependent(this.newDialog);

				            }
				
				            this.newDialog.open();
		},
		onCloseDialog:function(oEvent){
			this.newDialog.close();
			var oViewModel=this.getModel("worklistView");
			var that=this;
			that.onFunctionSet();
			that.onFunctionEnable();

		},
		onFunctionSet(oEvent){
			var oViewModel=this.getModel("worklistView");
			oViewModel.setProperty("/fragment/Miktar","");
			oViewModel.setProperty("/fragment/HedefBarcode","");
		},
		onFunctionEnable(oEvent){
			var oViewModel=this.getModel("worklistView");
			oViewModel.setProperty("/enabledHedef",false)

		},
		onChangeKaynak:function(oEvent){
			var that=this,
			oViewModel=this.getModel("worklistView"),
			kmiktarKontrol=oViewModel.getProperty("/fragment/Miktar"),
			mengeKontrol=oViewModel.getProperty("/fragment/Menge");

			if(kmiktarKontrol==="" || kmiktarKontrol===0)
			{
				sap.m.MessageToast.show("Kaynak Miktar alanı boş veya 0 olamaz!");
				that.onFunctionEnable();
				return;
	
			}
			else if(mengeKontrol<kmiktarKontrol)
			{
				sap.m.MessageToast.show("Kaynak Miktar alanı Miktardan fazla olamaz!");
				kmiktarKontrol="";
				that.onFunctionEnable();
				return;
			}
			else{
				var oValue = oEvent.getParameter("value");
				if (oValue !== "") {
					that.getView().getModel("worklistView").setProperty("/enabledHedef", true);
				}
			}
			
			


			
		},
		onSubmitHedef:function(oEvent){
			var that=this,
			oViewModel=this.getModel("worklistView"),
			oDataModel = this.getModel(),
			 ilkMiktar=oViewModel.getProperty("/fragment/Menge"),
			 kaynakMiktar=oViewModel.getProperty("/fragment/Miktar"),
			 hedefBarkod=oViewModel.getProperty("/fragment/HedefBarcode"),
			 lqnum=oViewModel.getProperty("/fragment/Lqnum");
			if (hedefBarkod === "" || hedefBarkod === undefined) {
				sap.m.MessageToast.show("Hedef Barkod alanı boş olamaz!");
				return;
			}
			  oDataModel.callFunction("/MoveToTargetBarcode", {
				urlParameters: {
					"Lqnum": lqnum,
					"Quantity": kaynakMiktar,
					"TargetBarcode": hedefBarkod
				}, 
				success: function () { 
					var guncelMenge = (ilkMiktar - kaynakMiktar);
					oViewModel.setProperty("/fragment/Menge", guncelMenge);
					that.onFunctionSet();
					if(parseInt(guncelMenge)===0){
						var that=this;
						that.onCloseDialog();
					}


				 },
				error: function () {
					
				}
			}); 


		} 
		

	});
});
